EntityFieldQuery Views Bulk Operations
=======================================

This is a simple Drupal extension that enables the use of Views Bulk Operations
on Views powered by the EntityFieldQuery Views query backend.

There is no configuration required; install and enable this module like any
other Drupal extension and bulk operations should become available on your EFQ
Views.
