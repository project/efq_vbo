<?php

/**
 * @file
 * Views hooks and functions for the EFQ VBO module.
 */

/**
 * Implements hook_views_data_alter().
 */
function efq_vbo_views_data_alter(&$data) {
  foreach (entity_get_info() as $entity_type => $info) {
    if (isset($data['efq_' . $entity_type])) {
      $data['efq_' . $entity_type]['views_bulk_operations'] = array(
        'title' => $data['efq_' . $entity_type]['table']['group'],
        'group' => t('Bulk operations'),
        'help' => t('Provide a checkbox to select the row for bulk operations.'),
        'real field' => $info['entity keys']['id'],
        'field' => array(
          'handler' => 'efq_vbo_handler_field_operations',
          'click sortable' => FALSE,
        ),
      );
    }
  }
}
